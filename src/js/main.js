// слайдер
const swiper = new Swiper('.swiper-container', {
  loop: true,
  // pagination: {
  //   el: '.swiper-pagination',
  // },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  scrollbar: {
    el: '.swiper-scrollbar',
    draggable: true
  },
  slideToClickedSlide: true,
});

// модальное окно
const alertContent = document.querySelector('.alert-page-content');
const alertShadow = document.querySelector('.alert-page-shadow');
const alertToggle = document.querySelectorAll('.alert-toggle');
alertToggle.forEach(i => {
  i.addEventListener('click', e => {
    e.preventDefault();
    alertShadow.classList.toggle('bury');
    alertContent.classList.toggle('bury');
  })
})

// меню
const burger = document.querySelectorAll('.header-mobile-burger');
const menu = document.querySelector('.header-mobile-menu');
burger.forEach(i => {
  i.addEventListener('click', e => {
    e.preventDefault();
    menu.classList.toggle('bury');
  })
})

// const button = document.querySelector('.menu-btn')
// const burgerButton = document.querySelector('.burger')
// const closeButton = document.querySelector('.close')
// const collapse = document.querySelector('.header-mobile-collapse')
// const header = document.querySelector('.header')
// button.addEventListener('click', e =>{
//   e.preventDefault();
//   burgerButton.classList.toggle('bury');
//   closeButton.classList.toggle('bury');
//   collapse.classList.toggle('bury');
//   header.classList.toggle('white');
// })


// // валидация формы
// const groupArr = document.querySelectorAll('.mail-form-group');
// const sbmtBtn = document.querySelector('.btn');
// groupArr.forEach(i => {
//   i.addEventListener('click', e => {
//     e.preventDefault();
//     i.firstElementChild.firstElementChild.classList.add('fog');
//     let input = i.firstElementChild.lastElementChild;
//     let block = i.firstElementChild;
//     let label = i.firstElementChild.firstElementChild;
//     input.classList.remove('bury');
//     input.focus();
//     block.style.padding = '6px 0 0 16px';
//     label.style.fontSize = '14px';
//         input.addEventListener('blur', e => {
//       e.preventDefault();
//       if (input.value.length < 1) {
//         i.lastElementChild.classList.remove('bury');
//         i.firstElementChild.classList.add('error');
//       }
//     })
//     input.addEventListener('input', e => {
//       e.preventDefault();
//       if (input.value.length > 0) {
//         i.lastElementChild.classList.add('bury');
//         i.firstElementChild.classList.remove('error');
//       }
//     })
//     let emailBlock = document.querySelector('.email-block');
//     let errorMsg = document.querySelector('.invalid-email');
//     emailBlock.lastElementChild.addEventListener('input', e => {
//       e.preventDefault();
//       if (!emailBlock.lastElementChild.value.match(/.+@.+\..+/i)) {
//         errorMsg.classList.remove('bury');
//         emailBlock.classList.add('error');
//       } else {
//         errorMsg.classList.add('bury');
//         emailBlock.classList.remove('error');
//       }
//     })
//   })
// })

// sbmtBtn.addEventListener('click', e => {
//   e.preventDefault();
//   groupArr.forEach(i => {
//     let input = i.firstElementChild.lastElementChild;
//     if (input.value.length < 1) {
//       i.lastElementChild.classList.remove('bury');
//       i.firstElementChild.classList.add('error');
//     }
//   })
// })

